<?php

use Illuminate\Database\Seeder;

class DrugsTableSeeder extends Seeder
{
    /**
     * DB table name
     *
     * @var string
     */
    protected $table;
    
    /**
     * CSV filename
     *
     * @var string
     */
    protected $filename;

    public function __construct()
    {
        $this->table = 'drugs';
        $this->filename = base_path() . '/dados.csv';
    }

    /**
     * Collect data from a given CSV file and return as array
     *
     * @param $filename
     * @param string $deliminator
     * @return array|bool
     */
    private function seedFromCSV($filename, $delimitor = ",")
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimitor)) !== false) {
                if (!$header) {
                    array_push($row, 'created_at', 'updated_at');
                    $header = $row;
                } else {
                    array_push($row, now(), now());
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->delete();
        $seedDrugs = $this->seedFromCSV($this->filename, ',');
        DB::table($this->table)->insert($seedDrugs);
    }
}
