@extends('layouts.app')

@section('content')
<div class="container" id="app">
    <drug-list :user="{{ auth()->user() }}"></drug-list>
</div>
@endsection