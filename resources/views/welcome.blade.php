@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h4>Make login with these credentials</h4>
                    Login: <span>user@memed.com.br</span><br>
                    Pass: <span>secret</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
