<?php

namespace App\Http\Controllers;

use App\Drug;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use App\Http\Requests\DrugCreateRequest;
use App\Http\Requests\DrugUpdateRequest;

class DrugController extends Controller
{
    protected $model;

    public function __construct(Drug $drug)
    {
        $this->model = new Repository($drug);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->model->paginate(20);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DrugCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DrugCreateRequest $request)
    {
        return $this->model->create($request->only($this->model->getModel()->fillable));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $drug = $this->model->show($id);
        $audit = $drug->audits()->with('user')->get();
        
        return compact('drug', 'audit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DrugUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DrugUpdateRequest $request, $id)
    {
        $this->model->update($request->only($this->model->getModel()->fillable), $id);
        return $this->model->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $drug
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return ($this->model->delete($id)) ? response()->json(['success' => 'Item deleted successfully!']) : response()->json(['message' => 'Item not deleted! Try later!']);
    }

    /**
     * Search text
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $query = $this->model->query();

        $search = request('s');

        $query->when(request('s', null), function ($querySearch) use ($search) {
            $querySearch->where('ggrem', 'like', "%$search%")
                ->orWhere('nome', 'like', "%$search%");
        });

        return $query->paginate(20);
    }
}
