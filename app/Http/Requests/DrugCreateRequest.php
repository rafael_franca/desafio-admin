<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DrugCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ggrem' => 'required|unique:drugs,ggrem|integer',
            'nome' => 'required|string',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'ggrem.required' => 'O GGREM é obrigatório',
            'ggrem.unique' => 'O GGREM informado já está cadastrado',
            'ggrem.integer' => 'O GGREM deve ser apenas números',
            'nome.required' => 'O nome do medicamento é obrigatório'
        ];
    }
}
