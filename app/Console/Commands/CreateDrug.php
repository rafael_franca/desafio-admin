<?php

namespace App\Console\Commands;

use App\Drug;
use App\Repositories\Repository;
use Illuminate\Console\Command;
use Validator;

class CreateDrug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'drug:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Drug';

    /**
     * The model.
     *
     * @var Model
     */
    protected $model;

    /**
     * Create a new command instance.
     *
     * @param  Drug  $drug
     * @return void
     */
    public function __construct(Drug $drug)
    {
        parent::__construct();
        $this->model = new Repository($drug);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'ggrem' => null,
            'nome' => null
        ];

        $data['ggrem'] = $this->validateCmd(function () {
            return $this->ask('What is the GGREM code for the new drug?');
        }, ['ggrem','required|unique:drugs,ggrem|integer']);

        $data['nome'] = $this->validateCmd(function () {
            return $this->ask('What is the name of the new drug?');
        }, ['nome','required|string']);

        try {
            $this->model->create($data);
            return $this->info('Medication successfully registered!');
        } catch (\Exception $e) {
            return $this->error('Oops! Something went wrong!');
        }
    }

    /**
     * Validate an input.
     *
     * @param  mixed   $method
     * @param  array   $rules
     * @return string
     */
    public function validateCmd($method, array $rules)
    {
        $value = $method();
        $validate = $this->validator($rules, $value);

        if ($validate !== true) {
            $this->warn($validate);
            $value = $this->validateCmd($method, $rules);
        }
        return $value;
    }

    /**
     * Make validator.
     *
     * @param  array   $rules
     * @param  mixed   $value
     * @return string
     */
    public function validator(array $rules, $value)
    {
        $validator = Validator::make([
            $rules[0] => $value
        ], [
            $rules[0] => $rules[1]
        ], [
            'ggrem.required' => 'The GGREM code is required',
            'ggrem.unique' => 'The GGREM code entered is already registered. Try another',
            'ggrem.integer' => 'The GGREM code should be just numbers',
            'nome.required' => 'The drug name is required'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $error->first($rules[0]);
        } else {
            return true;
        }
    }
}
