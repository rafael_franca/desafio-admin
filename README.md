![cabecalho memed desafio](https://user-images.githubusercontent.com/2197005/28128758-3b0a0626-6707-11e7-9583-dac319c8b45b.png)

# Desafio do Admin

## Instalando:

Para rodar o sistema, você precisa garantir que seu servidor atenda aos seguintes requisitos:

- PHP >= 7.1.3
- Extensão do PHP: OpenSSL
- Extensão do PHP: PDO
- Extensão do PHP: Mbstring
- Extensão do PHP: Tokenizer
- Extensão do PHP: XML
- Extensão do PHP: Ctype
- Extensão do PHP: JSON
- Composer

Em seguida, execute os seguintes passos:

1. Altere o arquivo `.env` de acordo com seus dados do servidor web
2. Execute o comando `composer install --optimize-autoloader`, para instalar todos os requisitos
3. Execute o comando `php artisan key:generate`, para gerar a chave criptográfica do sistema
4. Execute o comando `php artisan migrate --seed`, para migrar os dados para o seu banco de dados. Os dados dos medicamentos, também serão migrados, sem necessidade de intervenção.

## Usando:

Ao migrar o banco de dados, será criado um usuário de e-mail `user@memed.com.br` e senha `secret`, que será utilizado para acessar o sistema.

Você poderá visualizar, editar e excluir os medicamentos. O registro de auditoria será gerado automaticamente.

## Informações Extras:

Pelo carater de exibir apenas o sistema, não foi criado autenticação para a API REST, nem um SPA (Single Page Application), como deveria ser.

Foi criado um comando para acrescentar os medicamentos direto pelo terminal, para tanto, execute o comando `php artisan drug:create`, e insira as informações conforme forem solicitadas. Lembrando que, também há validação dos dados antes de fazer sua inserção.

## Agradecimentos

Obrigado pela oportunidade.

Espero poder trabalhar com vocês muito em breve :)

Let's change the world.. together!
